const http = require("http");
const fs = require("fs");
const url = require("url");

class AppUtils {
  constructor(request, response) {
    this.request = request;
    this.response = response;
    ({ pathname: this.pathname, query: this.query } = url.parse(
      request.url,
      true
    ));
  }

  getFile(path) {
    fs.readFile(`./${path}`, (error, content) => {
      if (error) {
        this.response.writeHead(400, { "Content-type": "text/html" });
        this.response.end(`Not files with name "${path}" found`);
        return;
      }

      this.response.writeHead(200, { "Content-type": "text/html" });
      this.response.end(content);
      this.writeLog(`Getting file from '${path}'`);
    });
  }

  writeFile(query) {
    let { filename, content } = query;

    try {
      if (!filename || !content) {
        throw "Bad input argumens";
      }

      if (!fs.existsSync("./file")) {
        fs.mkdirSync("./file");
      }

      fs.writeFile(`./file/${filename}`, content, "utf8", (err) => {
        if (err) {
          throw err;
        }

        this.response.writeHead(200, { "Content-type": "text/html" });
        this.response.end(
          `The file has been saved with the name "${filename}"`
        );
        this.writeLog(`New file with name '${filename}' saved`);
      });
    } catch (error) {
      this.response.writeHead(400, { "Content-type": "text/html" });
      this.response.end(error);
    }
  }

  getLog(query) {
    if (Object.entries(query).length !== 0) {
      this.getFilteredLog(query);
      return;
    }

    fs.readFile(`./logs/log.json`, (error, content) => {
      if (error) {
        this.response.writeHead(400, { "Content-type": "text/html" });
        this.response.end("Log file was not found");
        return;
      }

      this.response.writeHead(200, { "Content-type": "application/json" });
      this.response.end(content);
    });
  }

  getFilteredLog(query) {
    const from = Number.parseInt(query.from);
    const to = Number.parseInt(query.to);

    try {
      if (!from || !to) {
        throw "Bad filter argumens";
      }

      const json = this.readJson();
      json.logs = json.logs.filter(
        (value) => value.time >= from && value.time <= to
      );

      this.response.writeHead(200, { "Content-type": "application/json" });
      this.response.end(JSON.stringify(json, null, ' '));
    } catch (error) {
      this.response.writeHead(400, { "Content-type": "text/html" });
      this.response.end(error);
    }
  }

  writeLog(message) {
    const prevContent = this.readJson();
    prevContent.logs.push({
      message: message,
      time: Date.now(),
    });

    const content = JSON.stringify(prevContent, null, " ");

    if (!fs.existsSync("./logs")) {
      fs.mkdirSync("./logs");
    }

    fs.writeFile("./logs/log.json", content, "utf8", (error) => {
      if (error) {
        console.error("Occurred an error during writing log");
      }
    });
  }

  readJson() {
    try {
      if (!fs.existsSync("./logs/log.json")) {
        return {
          logs: [],
        };
      }

      return JSON.parse(fs.readFileSync("./logs/log.json", "utf8"));
    } catch (error) {
      console.error(error);
    }
  }
}

function server(request, response) {
  const utils = new AppUtils(request, response);
  const method = request.method;

  if (method === "GET") {
    if (utils.pathname === "/logs") {
      utils.getLog(utils.query);
    } else {
      utils.getFile(utils.pathname);
    }
  }

  if (method === "POST") {
    utils.writeFile(utils.query);
  }
}

module.exports = () => {
  http.createServer(server).listen(process.env.PORT || 8080);
};